<?php  

require("../Models/player.php");

function select_test(){
	$p = new Player;
	$p->name = "Richard";
	$p->born = "1980-05-20";
	$p->select();
	return $p;
}

function update_test(){
	$p = select_test();
	$p->dropouts = 10;
	$p->save();
}

function insert_test(){
	$p = new Player();
	$p->fill("Richard", "1980-05-20", 50, 2, "basket");
	$p->save();
}

function select_all_test(){
	$p = new Player();
	$players = $p->all();
	var_dump($players);
}

function delete_test(){
	$p = new Player();
	$p->name = "Richard";
	$p->born = "1980-05-20";
	$p->delete();
}

insert_test();
update_test();
select_all_test();
delete_test();

?>