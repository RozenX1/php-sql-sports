<?php  

require("../Models/contest.php");

function select_test(){
	$c = new Contest;
	$c->city = "Malta";
	$c->year = 2010;
	$c->select();
	return $c;
}

function update_test(){
	$c = select_test();
	$c->period = 20;
	$c->save();
}

function insert_test(){
	$c = new Contest();
	$c->fill("Malta", 2010, 10, 20000, 23000);
	$c->save();
}

function select_all_test(){
	$c = new Contest();
	$contests = $c->all();
	var_dump($contests);
}

function delete_test(){
	$c = new Contest();
	$c->city = "Malta";
	$c->year = 2010;
	$c->delete();
}

insert_test();
update_test();
select_all_test();
delete_test();


?>