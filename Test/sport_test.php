<?php  

require("../Models/sport.php");

function select_test(){
	$s = new Sport;
	$s->name = "soccer";
	$s->select();
	return $s;
}

function update_test(){
	$s = select_test();
	$s->ball = true;
	$s->save();
}

function insert_test(){
	$s = new Sport();
	$s->fill("soccer", false, "pitcher", false);
	$s->save();
}

function select_all_test(){
	$s = new Sport();
	$sports = $s->all();
	var_dump($sports);
}

function delete_test(){
	$s = new Sport();
	$s->name = "soccer";
	$s->delete();  
}

insert_test();
update_test();
select_all_test();
delete_test();

?>