<?php  

$sport_all = <<<SQL
	SELECT * FROM SPORTS;
SQL;

$sport_select = <<<SQL
	SELECT * FROM SPORTS
	WHERE name = ?;
SQL;

$sport_delete = <<<SQL
	DELETE FROM SPORTS
	WHERE name = ?;
SQL;

$sport_update = <<<SQL
	UPDATE SPORTS
	SET ball = ?, terrain = ?, team = ?
	WHERE name = ?;
SQL;

$sport_insert = <<<SQL
	INSERT INTO SPORTS
	VALUES (?,?,?,?); 
SQL;

$sport_all_played = <<<SQL
	SELECT S.* FROM SPORTS AS S
	WHERE EXISTS(
		SELECT * FROM PLAYERS AS P
		WHERE S.name = P.sport_name
		LIMIT 1);
SQL;

$sports_from_contest = <<<SQL
	SELECT DISTINCT S.* FROM SPORTS AS S 
	INNER JOIN PLAYERS AS PL 
	ON S.name = PL.sport_name
	INNER JOIN PARTICIPATIONS AS P 
	ON PL.name = P.player_name AND
		PL.born = P.player_born
	INNER JOIN CONTESTS AS C
	ON C.city = P.contest_city AND 
		C.year = P.contest_year 
	WHERE C.city = ? AND C.year = ?;
SQL;


define("SPORT_ALL_SQL", $sport_all);
define('SPORT_SELECT_SQL', $sport_select);
define('SPORT_DELETE_SQL', $sport_delete);
define('SPORT_UPDATE_SQL', $sport_update);
define('SPORT_INSERT_SQL', $sport_insert);
define('SPORT_ALL_PLAYED_SQL', $sport_all_played);
define("SPORTS_FROM_CONTEST_SQL", $sports_from_contest);

?>