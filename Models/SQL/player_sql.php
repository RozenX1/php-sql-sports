<?php  

$player_all = <<<SQL
	SELECT * FROM PLAYERS;
SQL;

$player_select = <<<SQL
	SELECT * FROM PLAYERS
	WHERE name = ? AND born = ?;
SQL;

$player_delete = <<<SQL
	DELETE FROM PLAYERS
	WHERE name = ? AND born = ?;
SQL;

$player_update = <<<SQL
	UPDATE PLAYERS
	SET wins = ?, dropouts = ?, sport_name = ?
	WHERE name = ? AND born = ?;
SQL;

$player_insert = <<<SQL
	INSERT INTO PLAYERS
	VALUES (?,?,?,?,?);
SQL;

$players_from_contest = <<<SQL
	SELECT P.* FROM PLAYERS AS P
	INNER JOIN PARTICIPATIONS AS PA
	ON P.name = PA.player_name AND 
		P.born = PA.player_born
	INNER JOIN CONTESTS AS C 
	ON C.city = PA.contest_city AND
		C.year = PA.contest_year
	WHERE C.city = ? AND C.year = ?;
SQL;

$players_from_sport = <<<SQL
	SELECT P.* FROM PLAYERS AS P
	INNER JOIN SPORTS AS S 
	ON S.name = P.sport_name
	WHERE S.name = ?;
SQL;

$players_born_union_contest_year= <<<SQL
	SELECT YEAR(P.born) FROM PLAYERS AS P
	UNION
	SELECT C.year FROM CONTESTS AS C;
SQL;


define("PLAYER_ALL_SQL", $player_all);
define('PLAYER_SELECT_SQL', $player_select);
define('PLAYER_DELETE_SQL', $player_delete);
define('PLAYER_UPDATE_SQL', $player_update);
define('PLAYER_INSERT_SQL', $player_insert);
define('PLAYERS_FROM_CONTEST_SQL', $players_from_contest);
define('PLAYERS_FROM_SPORT_SQL', $players_from_sport);
define("PLAYERS_BORN_UNION_CONTEST_YEAR_SQL", $players_born_union_contest_year);

?>