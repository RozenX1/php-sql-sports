<?php  

$contest_all = <<<SQL
	SELECT * FROM CONTESTS;
SQL;

$contest_select = <<<SQL
	SELECT * FROM CONTESTS
	WHERE city = ? AND year = ?;
SQL;

$contest_delete = <<<SQL
	DELETE FROM CONTESTS
	WHERE city = ? AND year = ?;
SQL;

$contest_update = <<<SQL
	UPDATE CONTESTS
	SET period = ?, costs = ?, incomes = ?
	WHERE city = ? AND year = ?;
SQL;

$contest_insert = <<<SQL
	INSERT INTO CONTESTS
	VALUES (?,?,?,?,?); 
SQL;

$contest_unique_per_year = <<<SQL
	SELECT C1.* FROM CONTESTS AS C1
	WHERE C1.year <> ALL(
		SELECT DISTINCT C2.year 
		FROM CONTESTS AS C2
		WHERE NOT(C1.city = C2.city 
			AND C1.year = C2.year));
SQL;

$contest_statistics_by_city = <<<SQL
	SELECT city, 
		avg(incomes), avg(costs),
		max(incomes), max(costs),
		min(incomes), min(costs)
	FROM CONTESTS
	GROUP BY city
	HAVING city = ?;
SQL;

define("CONTEST_ALL_SQL", $contest_all);
define('CONTEST_SELECT_SQL', $contest_select);
define('CONTEST_DELETE_SQL', $contest_delete);
define('CONTEST_UPDATE_SQL', $contest_update);
define('CONTEST_INSERT_SQL', $contest_insert);
define('CONTEST_UNIQUE_PER_YEAR_SQL', $contest_unique_per_year);
define('CONTEST_STATISTICS_BY_CITY', $contest_statistics_by_city);

?>