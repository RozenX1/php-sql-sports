<?php  

require("connection.php");
require_once("container.php");

abstract class Model extends Container{

	protected $con;

	abstract protected function inner_save();
	abstract protected function inner_delete();
	abstract protected function inner_all();
	abstract protected function inner_select();

	//$inner_func must be any of the inner functions above
	protected function wrapper_connection($inner_func, $params=[]){
		$this->con = new mysqli(HOST, USER, PW, DB);
		if($this->con->connect_error)
			return false;

		$result = call_user_func_array([$this, $inner_func], $params);
		$this->con->close();
		return $result;
	}

	public function save(){
		return $this->wrapper_connection('inner_save');
	}
	public function delete(){
		return $this->wrapper_connection('inner_delete');
	}
	public function all(){
		return $this->wrapper_connection('inner_all');
	}
	public function select(){
		return $this->wrapper_connection('inner_select');
	}

	private function bind($stmt, $params_type, $attrs){
		array_unshift($attrs, $params_type);
		$refs = array();
		foreach($attrs as $key => $value)
        	$refs[$key] = &$attrs[$key];
		call_user_func_array([$stmt, 'bind_param'], $refs);
	}

	protected function modify_execute($modify_sql, $params_type, $attrs){
		$modify = $this->con->prepare($modify_sql);
		$this->bind($modify, $params_type, $attrs);
		$result = $modify->execute();
		$modify->close();
		return $result;
	}

	protected function select_execute($select_sql, $params_type, $attrs){
		$select = $this->con->prepare($select_sql);
		$this->bind($select, $params_type, $attrs);
		$result = $select->execute();
		$fetch = $select->get_result();
		if($row=$fetch->fetch_array(MYSQLI_ASSOC)){
			foreach($row as $key => $value)
				$this->$key = $value;
			$this->selected = true;
		}
		$select->close();
		return $result;
	}

	protected function all_execute($select_all_sql, $caller_class, $params_type=NULL, $attrs=NULL){
		$all = $this->con->prepare($select_all_sql);
		if($params_type and $attrs)
			$this->bind($all, $params_type, $attrs);
		$all->execute();

		$list = array();
		$result = $all->get_result();
		while($row=$result->fetch_array(MYSQLI_ASSOC)){
			$element = new $caller_class;
			foreach($row as $key => $value)
				$element->$key = $value;

			$list[] = $element; 
		}
		$all->close();
		return $list;
	}

}

?>