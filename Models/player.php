<?php  

require("SQL/player_sql.php");
require_once('model.php');

class Player extends Model{

	public function fill($name, $born, $wins, $dropouts, $sport_name){
		$this->name = $name;
		$this->born = $born;
		$this->wins = $wins;
		$this->dropouts = $dropouts;
		$this->sport_name = $sport_name;
	}

	//REST
	protected function inner_save(){
		if($this->selected){
			return $this->modify_execute(PLAYER_UPDATE_SQL, "iisss", [$this->wins, $this->dropouts,
				$this->sport_name, $this->name, $this->born]);
		}else{
			return $this->modify_execute(PLAYER_INSERT_SQL, "ssiis", [$this->name, $this->born, 
				$this->wins, $this->dropouts, $this->sport_name]);
		}
	}

	protected function inner_delete(){
		return $this->modify_execute(PLAYER_DELETE_SQL, "ss", [$this->name, $this->born]);
	}

	protected function inner_all(){
		return $this->all_execute(PLAYER_ALL_SQL, get_class());
	}

	protected function inner_select(){
		return $this->select_execute(PLAYER_SELECT_SQL, "ss", [$this->name, $this->born]);
	}

	//SPECIAL QUERIES 
	public function from_contest($contest){
		return $this->wrapper_connection('inner_from_contest', [$contest]);
	}

	protected function inner_from_contest($contest){
		return $this->all_execute(PLAYERS_FROM_CONTEST_SQL, get_class(), "si", 
			[$contest->city, $contest->year]);
	}

	public function from_sport($sport){
		return $this->wrapper_connection('inner_from_sport', [$sport]);
	}

	protected function inner_from_sport($sport){
		return $this->all_execute(PLAYERS_FROM_SPORT_SQL, get_class(), "s",
			[$sport->name]);
	}

	public function born_union_contest_year(){
		return $this->wrapper_connection('inner_born_union_contest_year');
	}

	protected function inner_born_union_contest_year(){
		return $this->all_execute(PLAYERS_BORN_UNION_CONTEST_YEAR_SQL, 'Container');
	}
}

?>