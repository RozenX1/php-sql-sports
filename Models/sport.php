<?php  

require("SQL/sport_sql.php");
require_once("model.php");

class Sport extends Model{

	public function fill($name, $ball, $terrain, $team){
		$this->name = $name;
		$this->ball = $ball;
		$this->terrain = $terrain;
		$this->team = $team;
	}

	//REST
	protected function inner_save(){
		if($this->selected){
			return $this->modify_execute(SPORT_UPDATE_SQL, "isis", [$this->ball, $this->terrain,
				$this->team, $this->name]);
		}else{
			return $this->modify_execute(SPORT_INSERT_SQL, "sisi", [$this->name, $this->ball, 
				$this->terrain, $this->team]);
		}
	}

	protected function inner_delete(){
		return $this->modify_execute(SPORT_DELETE_SQL, "s", [$this->name]);
	}

	protected function inner_all(){
		return $this->all_execute(SPORT_ALL_SQL, get_class());
	}

	protected function inner_select(){
		return $this->select_execute(SPORT_SELECT_SQL, "s", [$this->name]);
	}

	//SPECIAL QUERIES
	public function all_played(){
		return $this->wrapper_connection("inner_all_played");
	}

	protected function inner_all_played(){
		return $this->all_execute(SPORT_ALL_PLAYED_SQL, get_class());
	}

	public function from_contest($contest){
		return $this->wrapper_connection('inner_from_contest', [$contest]);
	}	

	protected function inner_from_contest($contest){
		return $this->all_execute(SPORTS_FROM_CONTEST_SQL, get_class(), "si",
			[$contest->city, $contest->year]);
	}


}


?>