<?php  

require("SQL/contest_sql.php");
require_once('model.php');

class Contest extends Model{

	public function fill($city, $year, $period, $costs, $incomes){
		$this->city = $city;
		$this->year = $year;
		$this->period = $period;
		$this->costs = $costs;
		$this->incomes = $incomes;
	}

	//REST
	protected function inner_save(){
		if($this->selected){
			return $this->modify_execute(CONTEST_UPDATE_SQL, "iiisi", [$this->period, $this->costs,
				$this->incomes, $this->city, $this->year]);
		}else{
			return $this->modify_execute(CONTEST_INSERT_SQL, "siiii", [$this->city, $this->year, 
				$this->period, $this->costs, $this->incomes]);
		}
	}

	protected function inner_delete(){
		return $this->modify_execute(CONTEST_DELETE_SQL, "si", [$this->city, $this->year]);
	}

	protected function inner_all(){
		return $this->all_execute(CONTEST_ALL_SQL, get_class());
	}	

	protected function inner_select(){
		return $this->select_execute(CONTEST_SELECT_SQL, "si", [$this->city, $this->year]);
	}

	//SPECIAL QUERIES
	public function unique_per_year(){
		return $this->wrapper_connection('inner_unique_per_year');
	}

	protected function inner_unique_per_year(){
		return $this->all_execute(CONTEST_UNIQUE_PER_YEAR_SQL, get_class());
	}

	public function statistics_by_city(){
		return $this->wrapper_connection('inner_statistics_by_city');
	}

	protected function inner_statistics_by_city(){
		return $this->all_execute(CONTEST_STATISTICS_BY_CITY, 'Container', "s", 
			[$this->city]);
	}
}

?>